COLOR_VERMELLO := \033[0;31m
COLOR_Verde := \033[0;32m
COLOR_Azul := \033[0;34m
COLOR_Dorado = \033[38;2;218;165;32m
COLOR_Crema = \033[38;2;240;243;189m
COLOR_Menta = \033[38;2;2;195;154m
COLOR_Verde_persa = \033[38;2;0;168;150m
COLOR_Aquamarina = \033[38;2;2;128;144m
COLOR_Lapislazuli = \033[38;2;5;102;141m
COLOR_Amarelo = \033[0;33m
RESET = \033[0m

.PHONY: help # Muestra ayuda sobre los comandos disponibles
help: ## Muestra ayuda sobre los comandos disponibles
	@echo "Uso: make [comando]"
	@echo ""
	@echo "$(COLOR_Menta)Comandos:$(RESET)"
	@awk -F ':.*?## ' '/^[a-zA-Z0-9%._-]+:.*?## / {printf "  %-15s %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: install # Instala el script
install: ## Instala el script
	@ln -s $(shell pwd)/fssh.sh /usr/local/bin/fssh
	@chmod +x /usr/local/bin/fssh
	@mkdir -p $(HOME)/.config/fssh
	@cp .env.example $(HOME)/.config/fssh/.env
	@echo "$(COLOR_Verde)Script instalado.$(RESET)"

.PHONY: uninstall # Desinstala el script
uninstall: ## Desinstala el script
	@rm -f /usr/local/bin/fssh
	@echo "$(COLOR_VERMELLO)Script desinstalado.$(RESET)"
