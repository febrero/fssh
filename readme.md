# Conexión Segura a Servidores SSH

Este proyecto proporciona un script Bash diseñado para facilitar la conexión segura a servidores SSH. El script utiliza claves almacenadas en un archivo `.env` para configurar la conexión de manera eficiente y segura.

## Instrucciones de Uso

1. **Clonar el Repositorio:**

   ```bash
   git clone https://tu-repositorio.git
   cd tu-repositorio
   ``````

2. **Configurar Archivo `.env`:**

   Crea un archivo `.env` con la información de tus hosts SSH. Cada host debería tener una entrada similar a la siguiente:

   ```plaintext
   # Host de Desarrollo
   SSH_HOST_DESARROLLO=desarrollo.example.com
   SSH_PORT_DESARROLLO=22
   SSH_USERNAME_DESARROLLO=tu_usuario_desarrollo
   SSH_PRIVATE_KEY_PATH_DESARROLLO=/ruta/a/tu/clave_privada_desarrollo

   # Host de Producción
   SSH_HOST_PRODUCCION=produccion.example.com
   SSH_PORT_PRODUCCION=22
   SSH_USERNAME_PRODUCCION=tu_usuario_produccion
   SSH_PRIVATE_KEY_PATH_PRODUCCION=/ruta/a/tu/clave_privada_produccion
   ```

3.  **Ejecuta el script**
    
    Ejecuta el script utilizando el siguiente comando, reemplazando NOMBRE_DEL_HOST con el nombre representativo del host al que deseas conectarte:

    ```bash
    ./conectar_ssh.sh NOMBRE_DEL_HOST
    ```


## Opciones del Script

- **`-h`, `--help`:** Muestra la ayuda del script.
- **`-a`:** Otra forma de mostrar la ayuda.

## Requisitos

- Bash
- Archivo `.env` con la configuración de tus hosts SSH

## Instalar o Desinstalar el Script

Para instalar el script en el directorio `/usr/local/bin/`, ejecuta el siguiente comando:

```bash
make install
```

Para desinstlar
```bash
make install
```

## Notas Importantes

- Asegúrate de tener las claves privadas configuradas correctamente y con los permisos adecuados.
- Este script utiliza claves SSH en lugar de contraseñas para mayor seguridad.

---

**Autor:** Javier Fernández Febrero
