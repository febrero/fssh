#!/bin/bash
# Author: Javier Fernández Febrero


CONFIG_PATH="$HOME/.config/fssh"

# Cargar las variables
if [ -f "$CONFIG_PATH/.env" ]; then
  source "$CONFIG_PATH/.env"
else
  echo "Archivo .env no encontrado en $CONFIG_PATH."
  exit 1
fi

# Función para mostrar la ayuda
mostrar_ayuda() {
  echo "Uso: $0 [opciones]"
  echo "Conéctate a un servidor SSH utilizando claves SSH almacenadas en el archivo .env."

  # Mostrar información sobre los hosts desde el archivo .env
  for host_key in $(compgen -A variable | grep "SSH_HOST_"); do
    host_var="$host_key"
    user_var="${host_key/SSH_HOST/SSH_USERNAME}"

    host_name="${host_var//SSH_HOST_/}"
    host="${!host_var}"
    user="${!user_var}"

    # Verificar si todas las variables necesarias están definidas
    if [ -n "$host" ] && [ -n "$user" ]; then
      echo "  Host $host_name:"
      echo "    - Servidor SSH: $host"
      echo "    - Usuario: $user"
    fi
  done
}

# Función para conectar a un host SSH
conectar_ssh() {
  local host_var="SSH_HOST_$1"
  local port_var="SSH_PORT_$1"
  local user_var="SSH_USERNAME_$1"

  local host="${!host_var}"
  local port="${!port_var:-22}"
  local user="${!user_var}"

  # Verificar si todas las variables necesarias están definidas
  if [ -z "$host" ] || [ -z "$user" ]; then
    echo "Faltan variables de entorno necesarias para el host $1 en el archivo .env."
    return 1
  fi

  # Conectarse al servidor SSH utilizando claves SSH
  ssh -p "$port" "$user"@"$host"
}

# Opciones del script
while getopts ":h:a-:" option; do
   case $option in
      h) # Mostrar ayuda
         mostrar_ayuda
         exit;;
      a) # Mostrar ayuda alternativa
         mostrar_ayuda
         exit;;
      -) # Opciones de largo
         case "${OPTARG}" in
            help) # Ayuda extendida
               mostrar_ayuda
               exit;;
            *) # Opción no válida
               echo "Opción no válida."
               exit;;
         esac
         ;;
      \?) # Opción no válida
         echo "Opción no válida."
         exit;;
   esac
done

# Ejemplo de uso: conectar al Host especificado
if [ -n "$1" ]; then
  conectar_ssh "$1"
else
  echo "Uso: $0 <número_de_host>"
  echo "Ejecuta '$0 --help' para obtener más información."
fi

